from datetime import date


class ShopDrawing:
    def __init__(self):
        self.ID = 0
        self.Quote = ""
        self.Name = ""
        self.AssignDate = date.today()
        self.StartDate = date.today()
        self.CompleteDate = date.today()
        self.PageCount = 0
        self.Invoiced = False
        self.InvoiceNumber = 0
        self.Revised = False
        self.RevisedPageCount = 0
