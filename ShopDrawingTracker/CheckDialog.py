from PySide6.QtWidgets import (
    QDialog,
    QDialogButtonBox,
    QLabel,
    QVBoxLayout,
)


class CheckDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.setWindowTitle("Non-Invoiced Pages")

        q_btn = QDialogButtonBox.Close

        self.buttonBox: QDialogButtonBox = QDialogButtonBox(q_btn)
        self.buttonBox.rejected.connect(self.reject)
        self.layout: QVBoxLayout = QVBoxLayout()
        self.lbl_pages: QLabel = QLabel("How many pages aren't invoiced?")
        self.layout.addWidget(self.lbl_pages)
        self.layout.addWidget(self.buttonBox)
        self.setLayout(self.layout)

    def set_pages(self, pages, total):
        self.lbl_pages.setText(f"You have {pages} uninvoiced pages for a total of ${total}.")
