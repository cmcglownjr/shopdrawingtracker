from datetime import date
from PySide6 import QtCore
from PySide6.QtWidgets import QTextEdit, QDateEdit, QCheckBox
from PySide6.QtWidgets import QDialogButtonBox, QVBoxLayout, QFormLayout, QLabel, QWidget
from ShopDrawingTracker.ShopDrawing import ShopDrawing
import logging


logger = logging.getLogger(__name__)


class UpdateWindow(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.setWindowTitle('Update Data')
        btn_box = QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        self.buttonBox: QDialogButtonBox = QDialogButtonBox(btn_box)
        self.buttonBox.accepted.connect(self.apply_changes)
        self.buttonBox.rejected.connect(self.close)
        self.update_layout: QVBoxLayout = QVBoxLayout()
        lbl_title: QLabel = QLabel("Update Quote")
        lbl_title.setStyleSheet("font-weight: bold; font-size: 18pt")
        lbl_title.setAlignment(QtCore.Qt.AlignHCenter)
        form_layout: QFormLayout = QFormLayout()
        lbl_quote_number = QLabel("Quote Number:")
        lbl_name: QLabel = QLabel("Name:")
        lbl_assignment_date: QLabel = QLabel("Assignment Date:")
        lbl_start_date: QLabel = QLabel("Start Date:")
        lbl_complete_date: QLabel = QLabel("Completion Date:")
        lbl_page_count: QLabel = QLabel("Page Count:")
        lbl_invoiced: QLabel = QLabel("Invoiced:")
        lbl_invoice_number: QLabel = QLabel("Invoice #:")
        lbl_revised: QLabel = QLabel("Revised:")
        lbl_revise_page_count: QLabel = QLabel("Revised Page Count:")
        self.txt_quote_number: QTextEdit = QTextEdit()
        self.txt_quote_number.setMaximumWidth(200)
        self.txt_quote_number.setMaximumHeight(30)
        self.txt_name: QTextEdit = QTextEdit()
        self.txt_name.setMaximumWidth(200)
        self.txt_name.setMaximumHeight(30)
        self.dte_assign_date: QDateEdit = QDateEdit()
        self.dte_assign_date.setDisplayFormat("M/d/yyyy")
        self.dte_assign_date.setCalendarPopup(True)
        self.dte_start_date: QDateEdit = QDateEdit()
        self.dte_start_date.setDisplayFormat("M/d/yyyy")
        self.dte_start_date.setCalendarPopup(True)
        self.dte_start_date.setDate(date.today())
        self.dte_complete_date: QDateEdit = QDateEdit()
        self.dte_complete_date.setDisplayFormat("M/d/yyyy")
        self.dte_complete_date.setCalendarPopup(True)
        self.dte_complete_date.setDate(date.today())
        self.txt_page_count: QTextEdit = QTextEdit()
        self.txt_page_count.setMaximumWidth(100)
        self.txt_page_count.setMaximumHeight(30)
        self.chbx_invoiced: QCheckBox = QCheckBox()
        self.chbx_invoiced.setCheckState(QtCore.Qt.Unchecked)
        self.txt_invoice_number: QTextEdit = QTextEdit()
        self.txt_invoice_number.setMaximumWidth(100)
        self.txt_invoice_number.setMaximumHeight(30)
        self.chbx_revised: QCheckBox = QCheckBox()
        self.chbx_revised.setCheckState(QtCore.Qt.Unchecked)
        self.txt_revised_page_count: QTextEdit = QTextEdit()
        self.txt_revised_page_count.setMaximumWidth(100)
        self.txt_revised_page_count.setMaximumHeight(30)
        form_layout.addRow(lbl_quote_number, self.txt_quote_number)
        form_layout.addRow(lbl_name, self.txt_name)
        form_layout.addRow(lbl_assignment_date, self.dte_assign_date)
        form_layout.addRow(lbl_start_date, self.dte_start_date)
        form_layout.addRow(lbl_complete_date, self.dte_complete_date)
        form_layout.addRow(lbl_page_count, self.txt_page_count)
        form_layout.addRow(lbl_invoiced, self.chbx_invoiced)
        form_layout.addRow(lbl_invoice_number, self.txt_invoice_number)
        form_layout.addRow(lbl_revised, self.chbx_revised)
        form_layout.addRow(lbl_revise_page_count, self.txt_revised_page_count)
        self.update_layout.addWidget(lbl_title)
        self.update_layout.addLayout(form_layout)
        self.update_layout.addWidget(self.buttonBox)
        self.setLayout(self.update_layout)
        self.con = None
        self.cur = None
        self.shop = ShopDrawing()
        logger.debug(f'This is {__name__}')

    def set_database(self, con, cur):
        self.con = con
        self.cur = cur
        logger.info("Cursor has been set in the UpdateWindow.")

    def set_data(self, shop):
        self.shop = shop
        self.txt_quote_number.setText(self.shop.Quote)
        self.txt_name.setText(self.shop.Name)
        self.dte_assign_date.setDate(self.shop.AssignDate)
        self.dte_start_date.setDate(self.shop.StartDate)
        self.dte_complete_date.setDate(self.shop.CompleteDate)
        self.txt_page_count.setText(str(self.shop.PageCount))
        if self.shop.Invoiced:
            self.chbx_invoiced.setCheckState(QtCore.Qt.Checked)
        self.txt_invoice_number.setText(str(self.shop.InvoiceNumber))
        if self.shop.Revised:
            self.chbx_revised.setCheckState(QtCore.Qt.Checked)
        self.txt_revised_page_count.setText(str(self.shop.RevisedPageCount))

    def apply_changes(self):
        quote_number = self.txt_quote_number.toPlainText()
        name = self.txt_name.toPlainText()
        assignment_date = self.dte_assign_date.date().toPython()
        start_date = self.dte_start_date.date().toPython()
        complete_date = self.dte_complete_date.date().toPython()
        if self.txt_page_count.toPlainText() == "None":
            page_count = None
        else:
            page_count = self.txt_page_count.toPlainText()
        if self.chbx_invoiced.isChecked():
            invoiced = True
        else:
            invoiced = False
        if self.txt_invoice_number.toPlainText() == "None":
            invoice_number = None
        else:
            invoice_number = self.txt_invoice_number.toPlainText()
        if self.chbx_revised.isChecked():
            revised = True
        else:
            revised = False
        if self.txt_revised_page_count.toPlainText() == "None":
            revised_page_count = None
        else:
            revised_page_count = self.txt_revised_page_count.toPlainText()
        try:
            self.cur.execute("""UPDATE public."Main" SET quote = %s, name = %s, assign_date = %s, start_date = %s, 
            complete_date = %s, pages = %s, invoiced = %s, invoice_number = %s, revised = %s, revised_count = %s 
            WHERE id = %s""", (quote_number, name, assignment_date, start_date, complete_date, page_count, invoiced,
                               invoice_number, revised, revised_page_count, self.shop.ID))
            self.con.commit()
            logger.info(f"Quote # {quote_number} has been updated!")
        except Exception as err:
            logger.error(err)
        self.close()
