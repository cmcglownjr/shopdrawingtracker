#! /usr/bin/env python3

import ShopDrawingTracker.log
import sys
from os import path as os_path
import psycopg2
from configparser import ConfigParser
from PySide6 import QtCore
from PySide6.QtUiTools import QUiLoader
from PySide6.QtWidgets import QApplication, QPushButton, QTreeWidgetItem, QTreeWidget, QStatusBar
from PySide6.QtCore import QFile, QObject, Qt
import logging

from ShopDrawingTracker import DeleteDialog, InsertWindow, UpdateWindow, CheckDialog
from ShopDrawingTracker.ShopDrawing import ShopDrawing


logger = logging.getLogger(__name__)


def config(filename="ShopDrawingTracker/database.ini", section='postgresql'):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)

    # get section, default to postgresql
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return db


class GuiLogger(logging.Handler):
    def emit(self, record):
        self.edit.showMessage(self.format(record))  # implementation of append_line omitted


class Form(QObject):

    def __init__(self, ui_file, parent=None):
        super(Form, self).__init__(parent)
        ui_file = QFile(ui_file)
        ui_file.open(QFile.ReadOnly)

        loader = QUiLoader()
        self.window = loader.load(ui_file)
        ui_file.close()

        params = config()
        self.con = psycopg2.connect(**params)
        self.cur = self.con.cursor()
        self.insertWindow = None
        self.updateWindow = None
        self.deleteWindow = None
        self.checkWindow = None

        self.btnAdd: QPushButton | object = self.window.findChild(QPushButton, 'btnAdd')
        self.btnUpdate: QPushButton | object = self.window.findChild(QPushButton, 'btnUpdate')
        self.btnDelete: QPushButton | object = self.window.findChild(QPushButton, 'btnDelete')
        self.btnRefresh: QPushButton | object = self.window.findChild(QPushButton, 'btnRefresh')
        self.btnCheck: QPushButton | object = self.window.findChild(QPushButton, 'btnCheck')
        self.treeShopQuotes: QTreeWidget | object = self.window.findChild(QTreeWidget, 'treeShopQuotes')
        self.statusbar: QStatusBar | object = self.window.findChild(QStatusBar, 'statusbar')
        # Enable logging to QStatusBar object
        h = GuiLogger()
        h.edit = self.statusbar
        logger.addHandler(h)

        self.treeShopQuotes.setColumnCount(8)
        self.treeShopQuotes.setHeaderLabels(['Quote #', 'Name', 'Assignment Date', 'Start Date', 'Completion Date',
                                             'Pages', 'Invoiced', 'Invoice #'])
        self.btnAdd.clicked.connect(self.add_click)
        self.btnUpdate.clicked.connect(self.update_click)
        self.btnDelete.clicked.connect(self.delete_click)
        self.btnRefresh.clicked.connect(self.refresh_data)
        self.btnCheck.clicked.connect(self.check_data)
        self.treeShopQuotes.itemDoubleClicked.connect(self.id_check)

        self.refresh_data()
        self.window.show()

    def refresh_data(self):
        self.treeShopQuotes.clear()
        self.cur.execute("""SELECT * FROM public."Main" ORDER BY assign_date DESC, start_date DESC;""")
        query = self.cur.fetchall()
        for i in query:
            item = QTreeWidgetItem()
            item.setData(0, Qt.UserRole, i[2])
            item.setText(0, i[7])
            item.setText(1, i[5])
            if i[0] is None:
                item.setText(2, "Unassigned")
            else:
                item.setText(2, i[0].strftime("%Y-%m-%d"))
            if i[9] is None:
                item.setText(3, "Unassigned")
            else:
                item.setText(3, i[9].strftime("%Y-%m-%d"))
            if i[1] is None:
                item.setText(4, "Unassigned")
            else:
                item.setText(4, i[1].strftime("%Y-%m-%d"))
            item.setText(5, str(i[6]))
            if i[4]:
                item.setText(6, 'Yes')
            else:
                item.setText(6, 'No')
            item.setText(7, str(i[3]))
            self.treeShopQuotes.addTopLevelItem(item)

    def check_data(self):
        sql = """SELECT SUM(pages) FROM public."Main" WHERE invoiced IS FALSE AND revised IS FALSE;"""
        self.cur.execute(sql)
        query = self.cur.fetchall()
        if query[0][0] is None:
            total = 0
        else:
            total = int(query[0][0]) * 200
        try:
            self.checkWindow = CheckDialog.CheckDialog()
            self.checkWindow.set_pages(query[0][0], total)
            self.checkWindow.show()
        except Exception as err:
            logger.exception(f'An exception occurred during a check: {err}')

    def add_click(self):
        try:
            self.insertWindow = InsertWindow.InsertWindow()
            self.insertWindow.set_database(self.con, self.cur)
            self.insertWindow.show()
        except Exception as err:
            logger.exception(f'An exception occurred during a database addition: {err}')
        finally:
            self.refresh_data()
            logger.info("Refreshing the data.")

    def update_click(self):
        shop = ShopDrawing()
        try:
            shop_id = self.treeShopQuotes.currentItem().data(0, Qt.UserRole)
            shop.ID = shop_id
            self.cur.execute("""SELECT * FROM public."Main" WHERE id = %s""", [shop_id])
            query = self.cur.fetchall()
            shop.Quote = query[0][7]
            shop.Name = query[0][5]
            shop.AssignDate = query[0][0]
            if query[0][9] is None:
                shop.StartDate = None
            else:
                shop.StartDate = query[0][9]
            if query[0][1] is None:
                shop.CompleteDate = None
            else:
                shop.CompleteDate = query[0][1]
            if query[0][5] is None:
                shop.PageCount = 0
            else:
                shop.PageCount = query[0][6]
            shop.Invoiced = query[0][4]
            shop.InvoiceNumber = query[0][3]
            shop.Revised = query[0][8]
            shop.RevisedPageCount = query[0][10]
            self.updateWindow = UpdateWindow.UpdateWindow()
            self.updateWindow.set_database(self.con, self.cur)
            self.updateWindow.set_data(shop)
            self.updateWindow.show()
            logger.info("Refreshing the data.")
        except AttributeError:
            logger.warning(f'You must select an item to update first!')
        except Exception as err:
            logger.exception(f'An exception occurred during an update: {err}')
        finally:
            self.refresh_data()

    def delete_click(self):
        try:
            quote = self.treeShopQuotes.currentItem().text(0)
            quote_id = self.treeShopQuotes.currentItem().data(0, Qt.UserRole)
            self.deleteWindow = DeleteDialog.DeleteDialog()
            self.deleteWindow.set_quote(quote)
            if self.deleteWindow.exec_():
                self.cur.execute("""DELETE from public."Main" WHERE id = %s""", [quote_id])
                self.con.commit()
                logger.info(f"Quote # {quote} successfully deleted!")
            else:
                logger.info("Deletion canceled.")
        except AttributeError:
            logger.warning(f'You must select an item to delete first!')
        except Exception as err:
            logger.exception(f'An exception occurred during deletion: {err}')
        finally:
            self.refresh_data()

    def id_check(self):
        quote = self.treeShopQuotes.currentItem().text(0)
        name = self.treeShopQuotes.currentItem().text(1)
        pages = self.treeShopQuotes.currentItem().text(5)
        invoiced = self.treeShopQuotes.currentItem().text(6)
        quote_id = self.treeShopQuotes.currentItem().data(0, Qt.UserRole)
        logger.info(f"ID: {quote_id}, Quote #: {quote}, Name: {name}, Pages: {pages}, Invoiced: {invoiced}")


def main():
    logger.debug(f'This is {__name__}')

    def resource_path(relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            base_path = sys._MEIPASS
        except Exception:
            base_path = os_path.abspath(".")

        return os_path.join(base_path, relative_path)

    QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_ShareOpenGLContexts)
    mainwindow = resource_path('ShopDrawingTracker/MainWindow.ui')
    app = QApplication(sys.argv)
    form = Form(resource_path('ShopDrawingTracker/MainWindow.ui'))
    sys.exit(app.exec())


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()
